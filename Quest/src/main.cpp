#include "main.hpp"

#include <string>
#include <math.h>

#include "GlobalNamespace/PauseMenuManager.hpp"
#include "GlobalNamespace/LevelBar.hpp"
#include "GlobalNamespace/ScoreController.hpp"
#include "GlobalNamespace/ScoreModel.hpp"
#include "GlobalNamespace/RankModel.hpp"
#include "GlobalNamespace/RankModel_Rank.hpp"
#include "GlobalNamespace/LevelSelectionFlowCoordinator.hpp"

#include "questui/shared/BeatSaberUI.hpp"
#include "questui/shared/CustomTypes/Components/Backgroundable.hpp"

#include "UnityEngine/Canvas.hpp"
#include "UnityEngine/CanvasRenderer.hpp"
#include "UnityEngine/Vector3.hpp"
#include "UnityEngine/GameObject.hpp"
#include "UnityEngine/RenderMode.hpp"
#include "UnityEngine/UI/CanvasScaler.hpp"
#include "UnityEngine/CanvasRenderer.hpp"
#include "UnityEngine/MonoBehaviour.hpp"

#include "HMUI/ImageView.hpp"

static ModInfo modInfo; // Stores the ID and version of our mod, and is sent to the modloader upon startup

// Loads the config from disk using our modInfo, then returns it for use
Configuration& getConfig() {
    static Configuration config(modInfo);
    config.Load();
    return config;
}

// Returns a logger, useful for printing debug messages
Logger& getLogger() {
    static Logger* logger = new Logger(modInfo);
    return *logger;
}

// Called at the early stages of game loading
extern "C" void setup(ModInfo& info) {
    info.id = ID;
    info.version = VERSION;
    modInfo = info;
	
    getConfig().Load(); // Load the config file
    getLogger().info("Completed setup!");
}

std::string scoreStr;
std::string comboStr;
std::string rankStr;

MAKE_HOOK_OFFSETLESS(ScoreController_Update, void,
    GlobalNamespace::ScoreController* self
) {
    scoreStr = "";
    scoreStr += "SCORE\n";
    int score = self->baseRawScore;
    scoreStr += std::to_string(score);

    comboStr = "";
    comboStr += "COMBO\n";
    int combo = self->combo;
    comboStr += std::to_string(combo);

    rankStr = "";
    rankStr += "RANK\n";

    double percentage = 100*((double)score / (double)self->immediateMaxPossibleRawScore);
    int modifiedScore = GlobalNamespace::ScoreModel::GetModifiedScoreForGameplayModifiersScoreMultiplier(score, self->gameplayModifiersScoreMultiplier);
    int maxModifiedScore = GlobalNamespace::ScoreModel::GetModifiedScoreForGameplayModifiersScoreMultiplier(self->immediateMaxPossibleRawScore, self->gameplayModifiersScoreMultiplier);
    GlobalNamespace::RankModel::Rank rankModel = GlobalNamespace::RankModel::GetRankForScore(score, modifiedScore, self->immediateMaxPossibleRawScore, maxModifiedScore);

    Il2CppString* rank = GlobalNamespace::RankModel::GetRankName(rankModel);

    if (score != 0) {rankStr += to_utf8(csstrtostr(rank));}
    else { rankStr += "SS";}
    rankStr += " - ";
    if ( percentage == 0.0 ||std::isnan(percentage)) {rankStr += "0.0";} 
    else { rankStr += string_format("%.1f", percentage);}
    rankStr += "%";

    ScoreController_Update(self);
}

bool firstActivation = true;

TMPro::TextMeshProUGUI* score_text = nullptr;
TMPro::TextMeshProUGUI* combo_text = nullptr;
TMPro::TextMeshProUGUI* rank_text = nullptr;


MAKE_HOOK_OFFSETLESS(PauseMenuManager_ShowMenu, void,
    GlobalNamespace::PauseMenuManager* self
) {

    if (firstActivation) {
        UnityEngine::Transform* trans = self->levelBar->get_transform();
        UnityEngine::UI::HorizontalLayoutGroup* layout = QuestUI::BeatSaberUI::CreateHorizontalLayoutGroup(trans);
    
        self->levelBar->get_transform()->set_localPosition(UnityEngine::Vector3(0, 12.5, 0)); // level bar

        UnityEngine::Transform* backButtonTrans = self->backButton->get_transform();
        backButtonTrans->set_localPosition(backButtonTrans->get_localPosition() + UnityEngine::Vector3(0, -12.5, 0)); // offset backButton

        UnityEngine::Transform* continueButtonTrans = self->continueButton->get_transform();
        continueButtonTrans->set_localPosition(continueButtonTrans->get_localPosition() + UnityEngine::Vector3(0, -12.5, 0)); // offset continueButton

        UnityEngine::Transform* restartButtonTrans = self->restartButton->get_transform();
        restartButtonTrans->set_localPosition(restartButtonTrans->get_localPosition() + UnityEngine::Vector3(0, -12.5, 0));

        layout->get_transform()->set_localPosition(UnityEngine::Vector3(0, -22.5, 0));

        score_text = QuestUI::BeatSaberUI::CreateText(layout->get_transform(), "", true);
        score_text->set_alignment(TMPro::TextAlignmentOptions::Center);

        combo_text = QuestUI::BeatSaberUI::CreateText(layout->get_transform(), "", true);
        combo_text->set_alignment(TMPro::TextAlignmentOptions::Center);

        rank_text = QuestUI::BeatSaberUI::CreateText(layout->get_transform(), "", true);
        rank_text->set_alignment(TMPro::TextAlignmentOptions::Center);
        firstActivation = false;
    }

        score_text->set_text(il2cpp_utils::createcsstr(scoreStr));
        combo_text->set_text(il2cpp_utils::createcsstr(comboStr));
        rank_text->set_text(il2cpp_utils::createcsstr(rankStr));

    PauseMenuManager_ShowMenu(self);
}

MAKE_HOOK_OFFSETLESS(LevelSelectionFlowCoordinator_DidDeactivate, void,
    GlobalNamespace::LevelSelectionFlowCoordinator* self, bool removedFromHeirarchy, bool screenSystemDisabling
) {
    firstActivation = true;
    LevelSelectionFlowCoordinator_DidDeactivate(self, removedFromHeirarchy, screenSystemDisabling);
}

// Called later on in the game loading - a good time to install function hooks
extern "C" void load() {
    il2cpp_functions::Init();

    INSTALL_HOOK_OFFSETLESS(getLogger(), ScoreController_Update, il2cpp_utils::FindMethodUnsafe("", "ScoreController", "Update", 0));
    INSTALL_HOOK_OFFSETLESS(getLogger(), PauseMenuManager_ShowMenu, il2cpp_utils::FindMethodUnsafe("", "PauseMenuManager", "ShowMenu", 0));
    INSTALL_HOOK_OFFSETLESS(getLogger(), LevelSelectionFlowCoordinator_DidDeactivate, il2cpp_utils::FindMethodUnsafe("", "LevelSelectionFlowCoordinator", "DidDeactivate", 2));

    getLogger().info("Installing hooks...");
    // Install our hooks (none defined yet)
    getLogger().info("Installed all hooks!");
}